# Next Step Story PPW

[![pipeline status](https://gitlab.com/shafna.fikra/next-story/badges/master/pipeline.svg)](https://gitlab.com/shafna.fikra/next-story/-/commits/master)

[![coverage report](https://gitlab.com/shafna.fikra/next-story/badges/master/coverage.svg)](https://gitlab.com/shafna.fikra/next-story/-/commits/master)

Deployed at https://next-story-shafna.herokuapp.com/
